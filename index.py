from flask import Flask, redirect, url_for, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from pywebpush import webpush, WebPushException
import json
# {
#   "subject": "mailto:<someone@example.com>",
#   "publicKey": "BBfj1GYZGN7UyXpuD7t991lnUgp0X00lCIs1GykBdjtBlUq1ryByK9C_mYK6Hv6tnTba8OCN2eRBcA5xraNvbVc",
#   "privateKey": "KxAI_nxTmEg4Rl-VMQp8a8zNzvjqK2xPzClZJcuS1cI"
# }
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://"

db = SQLAlchemy(app)
class PushSubscription(db.Model):
    id = db.Column(db.Integer, primary_key=True, unique=True)
    subscription_json = db.Column(db.Text, nullable=False)

db.create_all()

@app.route('/')
def homepage():
    print("==============Welcome===========!")
    return render_template('index.html')

@app.route('/api/subscribe', methods=["POST"])
def subscribe():
    json_data = request.get_json('subscription_info')
    
    subscription = PushSubscription.query.filter_by(
        subscription_json=json_data['subscription_json']
    ).first()
    if subscription is None:
        subscription = PushSubscription(
            subscription_json=json_data['subscription_json']
        )
        db.session.add(subscription)
        db.session.commit()
    # print("id: {}".format(subscription.id))
    # print(subscription.subscription_json)
    return jsonify({
        "status": "success",
        "result": {
            "id": subscription.id,
            "subscription_json": subscription.subscription_json
        }
    })

@app.route('/api/notify_single', methods=["POST"])
def notify_single():
    json_data = request.get_json('subscription_info')
    subscription = {}
    subscription = json_data['subscription_info']
    # print("notify_signle: {}".format(subscription))
    results = trigger_push_notification(
        subscription,
        "Chillex",
        "Time to take a break bruh..."
    )
    return jsonify({
        "status": "success",
        "result": results
    })

@app.route('/api/notify', methods=["POST"])
def notify():
    # get all the subscription from the database
    subscriptions = PushSubscription.query.all()
    results = trigger_push_notifications_for_subscriptions(
        subscriptions,
        "Chillex",
        "Time to take a break bruh..."
    )
    return jsonify({
        "status": "success",
        "result": results
    })

@app.route('/sw.js', methods=['GET'])
def sw():
    return app.send_static_file('sw.js')

def trigger_push_notification(sub, title, body):
    VAPID_PRIVATE_KEY = "KxAI_nxTmEg4Rl-VMQp8a8zNzvjqK2xPzClZJcuS1cI"
    VAPID_CLAIM_EMAIL = "hello123@gmail.com"
    try:
        # print(json.loads(push_subscription.subscription_json))
        response = webpush(
            subscription_info=json.loads(sub),
            data=json.dumps({"title": title, "body": body}),
            vapid_private_key=VAPID_PRIVATE_KEY,
            vapid_claims={
                "sub": "mailto:{}".format(
                    VAPID_CLAIM_EMAIL)
            }
        )
        return response.ok
    except WebPushException as ex:
        if ex.response and ex.response.json():
            extra = ex.response.json()
            print("Remote service replied with a {}:{}, {}",
                extra.code,
                extra.errno,
                extra.message
            )
        print(ex)
        return False

# loop through all subscirptions and send all the clients a push notification
def trigger_push_notifications_for_subscriptions(subscriptions, title, body):
    print(subscriptions)
    return [trigger_push_notification(subscription.subscription_json, title, body)
            for subscription in subscriptions]

if __name__ == '__main__':
    app.run(debug=True)