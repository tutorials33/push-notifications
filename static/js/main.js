"use strict";

const applicationServerPublicKey =
    "BBfj1GYZGN7UyXpuD7t991lnUgp0X00lCIs1GykBdjtBlUq1ryByK9C_mYK6Hv6tnTba8OCN2eRBcA5xraNvbVc";
const pushButton = document.querySelector(".js-push-btn");
    let swRegistration = null;
let isSubscribed = false;
if ("serviceWorker" in navigator && "PushManager" in window) {
    console.log("Service Worker and Push is supported");

    navigator.serviceWorker
        .register("/sw.js")
        .then(function (swReg) {
            console.log("Service Worker is registered", swReg);
            console.log("scope: ", swReg.scope);

            swRegistration = swReg;
            initializeUI();
        })
        .catch(function (error) {
            console.error("Service Worker Error", error);
        });
} else {
    console.warn("Push messaging is not supported");
    pushButton.textContent = "Push Not Supported";
}


document
    .getElementById("push-btn-single")
    .addEventListener("click", function () {
        const subscription_json = localStorage.getItem("subscription_json");
        const data = { subscription_info: subscription_json };
        fetch("/api/notify_single", {
            method: "POST",
            body: JSON.stringify(data),
        });
        console.log("trigger push notification for single client");
    });

document
    .getElementById("push-btn-all")
    .addEventListener("click", function () {
        fetch("/api/notify", {
            method: "POST",
            body: {},
        });
        console.log("trigger push notification for all clients");
    });

function initializeUI() {
    pushButton.addEventListener("click", function () {
        pushButton.disabled = true;
        if (isSubscribed) {
            // TODO: Unsubscribe user
            unsubscribeUser();
        } else {
            subscribeUser();
        }
    });

    // Set the initial subscription value
    swRegistration.pushManager.getSubscription().then(function (subscription) {
        isSubscribed = !(subscription === null);

        updateSubscriptionOnServer(subscription);

        if (isSubscribed) {
            console.log("User IS subscribed.");
        } else {
            console.log("User is NOT subscribed.");
        }

        updateBtn();
    });
}

function subscribeUser() {
    const applicationServerKey = urlB64ToUint8Array(applicationServerPublicKey);
    swRegistration.pushManager
        .subscribe({
            userVisibleOnly: true,
            applicationServerKey: applicationServerKey,
        })
        .then(function (subscription) {
            console.log("User is subscribed.");

            updateSubscriptionOnServer(subscription);

            isSubscribed = true;

            updateBtn();
        })
        .catch(function (err) {
            console.log("Failed to subscribe the user: ", err);
            updateBtn();
        });
}

function unsubscribeUser() {
    swRegistration.pushManager
        .getSubscription()
        .then(function (subscription) {
            if (subscription) {
                return subscription.unsubscribe();
            }
        })
        .catch(function (error) {
            console.log("Error unsubscribing", error);
        })
        .then(function () {
            updateSubscriptionOnServer(null);

            console.log("User is unsubscribed.");
            isSubscribed = false;

            updateBtn();
        });
}

function updateSubscriptionOnServer(subscription) {
    // TODO: Send subscription to application server

    const subscriptionJson = document.querySelector(".js-subscription-json");
    const subscriptionDetails = document.querySelector(
        ".js-subscription-details"
    );

    if (subscription) {
        subscriptionJson.textContent = JSON.stringify(subscription);
        subscriptionDetails.classList.remove("is-invisible");
        fetch("/api/subscribe", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                subscription_json: JSON.stringify(subscription),
            }),
        })
            .then((response) => response.json())
            .then((data) => {
                //console.log(data);
                //console.log(data.result.subscription_json);
                localStorage.setItem(
                    "subscription_json",
                    data.result.subscription_json
                );
            });
    } else {
        subscriptionDetails.classList.add("is-invisible");
    }
}

function updateBtn() {
    if (Notification.permission === "denied") {
        pushButton.textContent = "Push Messaging Blocked.";
        pushButton.disabled = true;
        updateSubscriptionOnServer(null);
        return;
    }
    // console.log("====>");
    // console.log(isSubscribed);
    if (isSubscribed) {
        pushButton.textContent = "Disable Push Messaging";
    } else {
        pushButton.textContent = "Enable Push Messaging";
    }

    pushButton.disabled = false;
}

function urlB64ToUint8Array(base64String) {
    const padding = "=".repeat((4 - (base64String.length % 4)) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, "+")
        .replace(/_/g, "/");

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

